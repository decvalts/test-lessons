# -*- coding: utf-8 -*-
"""
Script to write a report on daily earthquake activity based on USGS data.
"""
from collections import Counter
import csv
from datetime import datetime
from math import floor
from pathlib import Path
from textwrap import dedent

import requests

print("*** THIS VERSION IS FOR MOVING FUNCTIONS INTO A CLASS ***")
print("*** IT IS STILL A WORK IN PROGRESS ***")


class EarthquakeReporter:
    def __init__(self, input_file):
        """Create an EarthquakeReporter class from csv file of data."""
        self.data = None  # This should be output of internal parsing functions
        # Not sure whether these should be functions, or init should call hidden
        # functions to store these values as attributes.
    
    def most_recent(self):
        """Extract date of most recent earthquake from internal data"""
        return max([row[0] for row in self.data])
    
    def largest(self):
        """Extract magnitude of largest earthquake from internal data"""
        return max([row[4] for row in self.data])
    
    def magnitude_counts(self):
        """Extract counts of magnitude classes from internal data"""
        # blah blah blah
        return
    
    def write_report(self, output_file):
        """Write report to output_file."""
        # blah blah blah
        pass


# See https://earthquake.usgs.gov/earthquakes/feed/v1.0/csv.php for details
DAILY_EQS_URL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.csv"  # noqa


# Get the csv data if it doesn't already exist
csv_data_file = Path('usgs_eqs_all_day.csv')
if not csv_data_file.exists():
    response = requests.get(DAILY_EQS_URL)
    csv_data_file.write_text(response.text)

csv_file_timestamp = datetime.fromtimestamp(csv_data_file.stat().st_ctime)


# Read the data from csv file into a list of lists
with open(csv_data_file) as infile:
    reader = csv.reader(infile)
    _ = next(reader)  # Discard first row
    all_data = [row for row in reader]


# Extract useful columns
data = []
for row in all_data:
    # Extract useful data
    timestamp, latitude, longitude, depth, mag = row[:5]

    # Write to new list, discarding magnitude < 0
    # We don't expect anything greater than magnitude 9
    assert float(mag) < 10, "Can't handle magnitude greater than 9."
    if float(mag) > 0:
        data.append((datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ'),
                     float(latitude), float(longitude),
                     float(depth), float(mag)))


# Find most recent earthquake
most_recent = max(row[0] for row in data)


# Find largest earthquake
largest = max(row[4] for row in data)


# Classify and count magnitudes (discarding less than 0 and grouping > 9)
def classify_magnitude(magnitude):
    """
    Returns text string for magnitude classification of earthquake.
    Assume magnitude 0 < 10.
    """
    # Assumes no magnitude ten
    mag_class_map = {
        0: "0-1",
        1: "1-2",
        2: "2-3",
        3: "3-4",
        4: "4-5",
        5: "5-6",
        6: "6-7",
        7: "7-8",
        8: "8-9",
        9: "> 9"
        }

    # Returns None for below magnitude 0
    classification = mag_class_map.get(floor(magnitude), None)
    return classification


magnitudes = [classify_magnitude(row[4]) for row in data]
magnitude_counts = Counter(magnitudes)


# Write report
header_info = dedent(f"""
    Earthquake Magnitude Report for {datetime.now().date().isoformat()}

    Data updated: {csv_file_timestamp}
    Most recent: {most_recent.isoformat()}
    Largest: {largest}

    Magnitude counts:
    """).strip()

with open('magnitudes_report.txt', 'w') as outfile:
    outfile.write(header_info + '\n')

    for mag_class in sorted(magnitude_counts.keys()):
        outfile.write(f"{mag_class}: {magnitude_counts[mag_class]: 3n}\n")
