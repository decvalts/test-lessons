# -*- coding: utf-8 -*-
"""
Script to write a report on daily earthquake activity based on USGS data.

This version is a single script using Pythonic idioms.
"""
from collections import Counter
from datetime import datetime
from math import floor
from pathlib import Path
from textwrap import dedent

from eq_report_utils import load_csv_data

# See https://earthquake.usgs.gov/earthquakes/feed/v1.0/csv.php for details
DAILY_EQS_URL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.csv"  # noqa
CSV_FILE = Path(__file__).parent / "usgs_eqs_all_day_example.csv"

# The load_csv_data function is defined in eq_report_utils.py
# It downloads and caches a csv file of data, then reads it into a list.
# Read the source code to learn about:
# - using Path to handle files
# - using requests module to transfer data via HTTP
# - next() to advance iterators
# - _ as a placeholder for unwanted variables
raw_data, last_updated = load_csv_data(DAILY_EQS_URL, CSV_FILE)


# Prepare data
data = []
for row in raw_data:
    # Extract useful data
    timestamp, latitude, longitude, depth, mag = row[:5]

    # Convert to Python data types
    try:
        converted_row = (datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ'),
                         float(latitude),
                         float(longitude),
                         float(depth),
                         float(mag),
                         )
    except ValueError:
        print(f"Bad data in row: {row}")

    # We don't expect anything greater than magnitude 9 so the he following
    # line will crash the program to warn the user.
    assert float(mag) < 10, "Can't handle magnitude greater than 9."

    # Write to new list, discarding magnitude < 0
    if float(mag) > 0:
        data.append(converted_row)


# Find largest earthquake
largest = max(row[4] for row in data)


# Classify and count magnitudes
def classify_magnitude(mag):
    """
    Assign numerical magnitude to size category.
    """
    # Assumes no magnitude ten
    mag_class_map = {
        0: "0-1",
        1: "1-2",
        2: "2-3",
        3: "3-4",
        4: "4-5",
        5: "5-6",
        6: "6-7",
        7: "7-8",
        8: "8-9",
        9: "> 9"
        }

    category = mag_class_map.get(floor(mag))
    return category


magnitudes = [classify_magnitude(row[4]) for row in data]
magnitude_counts = Counter(magnitudes)


# Write report
header_info = dedent(f"""
    Earthquake Magnitude Report for {datetime.now().date().isoformat()}

    Data updated: {last_updated}
    Largest: {largest}

    Magnitude counts:
    """).strip()

with open('magnitude_report.txt', 'w') as outfile:
    outfile.write(header_info + '\n')

    for mag_class in sorted(magnitude_counts.keys()):
        outfile.write(f"{mag_class}: {magnitude_counts[mag_class]: 3n}\n")
