---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

This lesson is a follow on to the Introduction to Python course at the BGS, or Python course for people
who already have some python knowledge and wish to increase this further. It will be taught using the
Spyder IDE (Integrated Development Environment), but attendees may use their interpreter or IDE of 
choice, and time will be allowed to discuss the various merits of each approach.

> ## Prerequisites
>
> 1.  Learners should have a background knowledge of Python **roughly** equivalent to the 
>     Introduction to Python course, but that course is not a prerequisite. As a guide, you 
>     will find the course easier if you already have a basic grasp of:
>     *   Variables, basic operators, basic Python keywords and statements.
>     *   Functions
>     *   Modules and how to import external libraries. 
>     *   Launching, and general familiarity with a Python IDE or development environment of your choice.
>     and how to start a Python interpreter.
>
> 2. Learners must install Python 3 before the class starts, and the following libraries:
>    *    Numpy
>    *    Pandas
>    *    Pytest
>    *    xarray
>    *    geopandas
>    *    cartopy
>
> 3. Learners must get the datafiles before the class starts:
>    please download and unzip the file
>    [python-intermediate-data.zip]({{page.root}}/files/python_intermediate_data.zip). 
>    One lesson also uses some example scripts, these can be downloaded here: [example-code.zip]({{page.root}}/files/example-code.zip).
>
>
>    Please see [the setup instructions][lesson-setup]
>    for details.
{: .prereq}

{% include links.md %}
