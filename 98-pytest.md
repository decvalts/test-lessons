---
title: "Test driven development with Pytest"
teaching: 20 
exercises: 45
questions:
- "How can I use the Pytest library for automated testing?"
---

## What is Pytest?

In the previous lesson, we looked at writing tests using the built in `assert` statements in Python.
Now we will look at a more feature-rich testing library called **Pytest**. Pytest is a third-party
testing framework designed to make it easier to write small tests, but also to scale these up to
larger programs and libraries.

With the Pytest library, you will write test scripts that are separate from your main Python program.
E.g. if you are writing a program called `my_program.py`, alongside it you will write a test script,
or *suite* of tests called `test_my_program.py`. Periodically you will then run your test suite to
make sure that new changes in your code have not broken any functionallity, and that new functionality
can be developed along the principles of test-driven development.

## First steps with Pytest

 
