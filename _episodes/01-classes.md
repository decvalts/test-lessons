---
title: "Object oriented programming"
teaching: 20 
exercises: 20
questions:
- "How can I abstract common tasks and data structures?"
objectives:
- "Understand when using object oriented programming may be useful."
- "Write my own Python objects to sotre data and methods."
keypoints:
- "Classes are the templates that let us define our own objects"
- "Objects combine data with functions in logical groupings."
- "Classes, like functions, can make code reusable and more modular"

---
## What is object-oriented programming?

*   Object-oriented programming is a way of grouping together data and related functions into a single structure, called an **object**.
*   Objects should group together functions and the data they operate on in a logical way, this is called **encapsulation**.
    * For example, you may have a model simulation that produces a list of data as one of its outputs. 
    * You may then use another function to plot the data and produce graphs and charts. 
    * The data structure (the list) and the plotting function, could be grouped together in a single **object** containing data and functions. 
*   This allows us to separate parts of our program into more reusable chunks, similar to the way in which we can write functions and modules. 


> ## Everything is an object in Python!
>
> Even if you have not written your own objects in Python before, you have probably already used Python's object oriented interface. In Python, *everything* is an object, such as built-in types, variables, string-literals, functions, and so on. If you have used the dot notation to access function calls before, this is object-oriented programming. For example:
>
> ~~~
> name = "Ada Lovelace"    # Create a string object and assign it to the variable `name`
> name.upper()             # Call the `upper()` method on the string object.
> ~~~
> {: .language-python}
{: .callout}


> ## The `dir()` function
> 
> Python has a built in function called `dir()` which can be used to quickly inspect what attributes are contained within an object. 
> The output can be very long, as sometimes there are many 'hidden' or special functions, but it can be useful to quickly remind yourself,
> or discover, what attributes are available with a given object.
>
> ~~~
> dir(name)
> ~~~
> {: .language-python}
> ~~~
> ['__add__', '__class__', '__contains__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isascii', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']
> ~~~
> {: .output}
>
> Can you spot some of the commonly-used string methods such as `split`, `upper` etc?
{: .callout}



> ## Objects have attributes, some of which are "callable"
>
> * Objects in Python contain attributes. We access an object's attributes using the dot notation (`object.attribute`).
> * Some of these attributes contain pieces of information, or data.
> * Objects can also have attributes that are **callable**, i.e. we can call on them to return a value or another object to us.
> * Callable attributes use the parentheses to indicate they are callable. (`object.function_call()`)
>
> ~~~
> complex_num = 4 + 2j
> print(complex_num.real)          # This is an attribute containing only data
> print(complex_num.imag)          # (as above)
> print(complex_num.conjugate())   # This object's attribute is callable (it is a function)
> ~~~
> {: .language-python}
> ~~~
> 4
> 2j
> (4 - 2j)
~~~
> {: .output}
{: .callout}


## Classes in Python

*   A **class** in Python is the template or blueprint used to define an object.
*   When we write a class in Python, we are specifying which functions and data we want to group together, and how the object will behave in our program.
*   Defining the class does not create an object, in the same way the blueprints do not build a house, only show us how it should be constructed. 

The simplest example of writing a class in Python is as follows:


~~~
class MyClass:
  pass
~~~
{: .language-python}

*   The class is given a name, like function would be given a name
*   The code inside the class must be indented. 
*   To finish the definition of the class, we unindent the code to the previous level of indentation.
*   A **pass** statement is required in this example to be a minimal working example. **pass** tells Python to procede to the next block.
    * The pass statement would not be needed if your class contained other code or variables.
    * So in effect, this class does nothing.


### A simple class in Python

*   Object oriented programming is a way of modelling real world things and the relationships between them.
    * For example a "Stratigraphic Log" object could have an array of data representing the thinckness of each stratigraphic layer, and a description of each layer.
    * The functions associated with this object could be used to calculate the total depth of sections of the log, or count how many stratigraphic units there are of a certain geology. 
    * The key point is to group together related functions and the data they are applied to, rahter than have them scattered throughout a large, unwieldly program or script.

~~~
class StratLog:
    """Object that will represent a stratigraphic log"""
    
    def __init__(self):
        """Initialise a new strat log"""
        self.lithologies = []
        self.depths = [0]
    
    def total_depth(self):
        """Return the total depth of the stratigraphic log"""
        return sum(self.depths)
~~~
{: .language-python}

    
*   The `__init__()` method is a special method in Python used to define how an object is initialised.
    * **Initialisation** refers to the state we want the object to be in when it is first created.
* In this `StratLog` class, there are two bits of data that the object contains:
    * `lithologies`
    * `depths`
* Every time we create a new `StratLog` object, these variables will be initialised to the ones given in `__init__()`.
* We can modify the values later once we have created an object.
* Our `StratLog` class has one function, or **method**, which uses the data specified in the class to return a result.
* The class definition defines how our Borehole object will work, and what data it contains, but it does not create any `StratLog` objects just yet...


> ## Python's *self* keyword
>
> ***self*** is a special keyword used in Python used within classes to refer to an instance of the object when it is created. (Actually any word could be used, but by common convention ***self*** is used almost exclusively in Python code when writing classes.)
{: .callout}


### Creating objects from the class

Now we have defined our class, we can create a `StratLog` object and explore it:

~~~
my_log = StratLog()
print(type(my_log))
~~~
{: .language-python}
~~~
__main__.StratLog
~~~
{: .output}

*   Put formally, the variable `my_log` is an object of `StratLog` type.
*   A **class** is a way of defining our own types of object in Python.
*   We must create an object first from the class first before using it.

Objects that we create using our own classes behave much like built in Python types.

We can call our `total_depth` method on our straigraphic log object to calculate the depth of the borehole. But as we have only an empty list of depths, we should expect the answer to be zero, so lets add some more depth measurements to our strat log object:

~~~
my_log.depths.append(4, 6, 3.3)
total = my_log.total_depth()
print(total)
~~~
{: .language-python}
~~~
13.3
~~~
{: .output}


### Initialising objects with `__init__()`

When our `StratLog` object is created, it does not contain any meaningful values and we have to add these separately in a separate step. It would be
useful if we could initialise the object ready with some values when we create the `my_log` variable.

* The `__init__()` special method determines how objects are initialised when created. 
* We can pass arguments to `__init__()` when our object is created. 

~~~
class StratLog:
    """Object that will represent a stratigraphic log"""
    
    def __init__(self, lithologies, depths):
        """Initialise a new stratlog"""
        self.lithologies = lithologies
        self.depths = depths
    
    def total_depth(self):
        """Return the total depth of the strata"""
        return sum(self.depths)

my_log = StratLog(lithologies=["Sandstone", "Shale"], depths=[4, 6, 3.3])
print(my_log.total_depth())
~~~
{: .language-python}
~~~
13.3
~~~
{: .output}

*   This time, we can pass values to our StratLog object when we create it.
*   If we try to create an empty `StratLog` object, Python will warn us with an error:

~~~
empty_log = StratLog()
~~~
{: .language-python}
~~~
Traceback (most recent call last):

  File "<ipython-input-5-b6c9267bf15b>", line 1, in <module>
    empty_log = StratLog()

TypeError: __init__() missing 2 required positional arguments: 'lithologies', and 'depths'
~~~
{: .output}


## Building more complex objects from smaller pieces

It is tempting when first writing objects in Python to extend classes with ever more data structures, attributes, and functions. Eventually, this style of class writing becomes unmanageble and the code is hard to change later on without breaking other features of the class/object. (Such an object is sometimes known as a [*God-object*](https://softwareengineering.stackexchange.com/questions/141958/how-to-refactor-a-python-god-class), as it knows too much and can do too many things, and is often considered a bad programming style.)

Suppose we wanted to represent not just a Startigraphic log, but a complete borehole. We could keep extending `StratLog` and rename it perhaps, or we could build it up from smaller, more manageable parts.

In object oriented programming, there are two approaches to this: **inheritance** and **composition**.

> ## Object composition
>
> Composition refers to building more complex objects out of smaller ones. Let's say we were writing a `Borehole` object that represented a drilling site with four cores at each site. We could construct our object like so:
>
> ~~~
> class Borehole():
>     
>    def __init__(self, bore_ID, bore_name):
>        self.bore_ID = bore_ID
>        self.bore_name = bore_name
>        
>        # Now add the logs
>        self.log1 = StratLog(lithologies=["Sandstone", "Shale"], depths=[4, 6, 3.3])
>        self.log2 = StratLog(lithologies=["Sandstone", "Shale"], depths=[4.2, 6, 2.3])
>        self.log3 = StratLog(lithologies=["Sandstone", "Shale"], depths=[3, 5.6, 3.3])
>        self.log4 = StratLog(lithologies=["Sandstone", "Shale"], depths=[4, 6, 3.3])
> ~~~
> {: .language-python}
{: .callout}

> ## Extending the Borehole class
> 
> 1. Write a method in the Borehole class that calculates the **total** depth of **all**
> the cores/logs combined. 
> 
> 2. Add another attribute to the Borehole class that represents the diameter of the core drill.
> Now use this to write a method that calculates the total **volume** of material extracted for
> each borehole. (Maybe this would help the core store people plan how much rack space they need?)
> Hint: The volume of a cylinder is given by πr^2h, where h is the depth (height) of the strat log.
>
> 3. At the moment, the individual logs are hard coded into the Borehole class. Can the code be
> rewritten so that you can initialise a `Borehole` object by passing it `StratLog` objects that
> you create first?
> 
{: .challenge}

<!--
> ## Extending the Borehole class
>
> You are given a text file representing a very simple borehole log. (Download here)
> Can you modify the `borehole` class above so that when a borehole object is created, 
> it is initialised by reading in the filename?
>
> ~~~
> my_borehole = Borehole("./boredata.txt")
> ~~~
> {: .language-python}
> > ## Solution
> >
> > ~~~
> > # Solution goes here
> > ~~~
> > {: .output}
> >
> > Soulution explanation.
>{: .solution}
{: .challenge}
-->

